﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application.Forms
{
    public partial class DbForm : Form
    {
        public DbForm()
        {
            InitializeComponent();
            Canceled = true;
        }

        public bool Canceled { get; private set; }
        public int Variant { get; private set; }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Canceled = false;
            if (radioButton1.Checked)
                Variant = 0;
            else
                Variant = 1;
            Close();
        }
    }
}
