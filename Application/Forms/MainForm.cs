﻿using Application.Forms;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Application
{
    public partial class MainForm : Form, IView
    {

        public MainForm()
        {
            InitializeComponent();
            toolStripComboBox1.SelectedIndex = 0;
            toolStripComboBox2.SelectedIndex = 0;
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.ScrollToCaret();
            richTextBox1.Refresh();
            RefreshControls();
            groupBox1.Location = new Point((groupBox1.Parent.Size.Width - groupBox1.Size.Width) / 2, (groupBox1.Parent.Size.Height - groupBox1.Size.Height) / 2);
            strBuilder = new StringBuilder();
        }

        public event EventHandler<FileEventArgs> reposCreated;
        public event EventHandler<FileEventArgs> reposOpened;
        public event EventHandler<FileEventArgs> reposSaved;
        public event Action reposClosed;
        public event Action<int> dbOpened;
        public event EventHandler<AddEventArgs> recordAdded;
        public event EventHandler<EditEventArgs> recordEdited;
        public event EventHandler<DeleteEventArgs> recordDeleted;
        public event EventHandler<CompileEventArgs> compile;
        public event EventHandler<CalculateEventArgs> calculate;
        
        private string filePath = "";
        private bool fileChanged = false;
        private bool dbIsOpened = false;
        private StringBuilder strBuilder;
        private int variant = 0;
        private int fileVariant = 0;

        public int Variant
        {
            get
            {
                return variant;
            }
            set
            {
                variant = value;
                toolStripComboBox1.SelectedIndex = variant;
                toolStripComboBox2.SelectedIndex = variant;
                labelResult.Text = "=";
                if (variant == 0)
                    labelSign.Text = "/";
                else
                    labelSign.Text = "x";
                if (tabControl1.SelectedIndex == 2)
                    RefreshStatusLabel(2);
            }
        }

        public void updateCalculate(string result)
        {
            labelResult.Text = $"= {result}";
        }

        public void updateLog(string message)
        {
            strBuilder.Append('\n' + message);
            richTextBox1.Text = strBuilder.ToString();
        }

        public void updateStructs(DataTable table)
        {
            dataGridView1.DataSource = table;
        }

        private void RefreshControls()
        {
            int selectedIndex = tabControl1.SelectedIndex;
            bool tab1 = selectedIndex == 0;
            bool tab2 = selectedIndex == 1;
            bool tab3 = selectedIndex == 2;
            bool fileOpened = filePath.Length > 0;
            if (tab1 && variant != fileVariant)
                Variant = fileVariant;
            toolStripCreate.Enabled = tab1;
            toolStripOpen.Enabled = tab1;
            toolStripSave.Enabled = tab1 && (fileOpened || dbIsOpened);
            toolStripSaveAs.Enabled = tab1 && fileOpened;
            toolStripAdd.Enabled = tab1 && (fileOpened || dbIsOpened);
            toolStripEdit.Enabled = tab1 && (fileOpened || dbIsOpened);
            toolStripDelete.Enabled = tab1 && (fileOpened || dbIsOpened);
            toolStripUndo.Enabled = tab2;
            toolStripRedo.Enabled = tab2;
            toolStripRun.Enabled = tab2 || tab3;
            toolStripComboBox1.Enabled = tab2 || tab3;
            toolStripComboBox2.Enabled = tab2 || tab3;
            файлToolStripMenuItem.Enabled = tab1;
            сохранитьToolStripMenuItem.Enabled = fileOpened || dbIsOpened;
            сохранитьКакToolStripMenuItem.Enabled = fileOpened;
            закрытьToolStripMenuItem.Enabled = fileOpened || dbIsOpened;
            добавитьToolStripMenuItem.Enabled = tab1 && (fileOpened || dbIsOpened);
            изменитьToolStripMenuItem.Enabled = tab1 && (fileOpened || dbIsOpened);
            удалитьToolStripMenuItem.Enabled = tab1 && (fileOpened || dbIsOpened);
            вырезатьToolStripMenuItem.Enabled = tab2;
            копироватьToolStripMenuItem.Enabled = tab2;
            вставитьToolStripMenuItem.Enabled = tab2;
            правкаToolStripMenuItem.Enabled = tab1 || tab2;
            открытьИзБДToolStripMenuItem.Enabled = tab1;
            RefreshStatusLabel(selectedIndex);
        }

        private void RefreshStatusLabel(int tabIndex)
        {
            switch (tabIndex)
            {
                case 0:
                    {
                        string fileName;
                        if (dbIsOpened)
                        {
                            int v = Variant == 0 ? 7 : 9;
                            fileName = $"База {v} вариант";
                        }
                        else
                        {
                            string[] pieces = filePath.Split('\\');
                            fileName = pieces[pieces.Length - 1];
                        }
                        if (!dbIsOpened && filePath == "")
                            statusLabel.Text = "Создайте или откройте файл";
                        else if (dataGridView1.SelectedRows.Count == 1)
                            statusLabel.Text = $"Строка {dataGridView1.SelectedRows[0].Index}  —  {fileName + (fileChanged ? "*" : "")}";
                        else
                            statusLabel.Text = fileName;
                        break;
                    }
                case 1:
                    {
                        statusLabel.Text = $"Строка {richTextBox2.GetLineFromCharIndex(richTextBox2.SelectionStart)}, Символ {richTextBox2.SelectionStart - richTextBox2.GetFirstCharIndexOfCurrentLine()}";
                        break;
                    }
                case 2:
                    {
                        statusLabel.Text = Variant == 0 ? "Деление" : "Умножение";
                        break;
                    }
            }
        }

        private void fileCreate_Click(object sender, EventArgs e)
        {
            createFileDialog.FileName = "";
            createFileDialog.ShowDialog();
        }

        private void fileCreate(object sender, CancelEventArgs e)
        {
            if (createFileDialog.FileName != "")
            {
                string[] pieces = createFileDialog.FileName.Split('.');
                string extension = pieces[pieces.Length - 1];
                if (extension == "csv")
                    Variant = 0;
                else if (extension == "bin")
                    Variant = 1;
                else
                    throw new NotImplementedException();
                fileVariant = Variant;
                filePath = createFileDialog.FileName;
                fileChanged = false;
                dbIsOpened = false;
                RepositoryConfig conf = new RepositoryConfig();
                conf.addParameter("FileName", filePath);
                reposCreated?.Invoke(this, new FileEventArgs(conf));
                RefreshControls();
            }
        }

        private void fileOpen_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = "";
            openFileDialog.ShowDialog();
        }

        private void fileOpen(object sender, CancelEventArgs e)
        {
            if (openFileDialog.FileName != "")
            {
                string[] pieces = openFileDialog.FileName.Split('.');
                string extension = pieces[pieces.Length - 1];
                if (extension == "csv")
                    Variant = 0;
                else if (extension == "bin")
                    Variant = 1;
                else
                    throw new NotImplementedException();
                fileVariant = Variant;
                filePath = openFileDialog.FileName;
                fileChanged = false;
                dbIsOpened = false;
                RepositoryConfig conf = new RepositoryConfig();
                conf.addParameter("FileName", filePath);
                reposOpened?.Invoke(this, new FileEventArgs(conf));
                RefreshControls();
            }
        }

        private void fileSave_Click(object sender, EventArgs e)
        {
            fileChanged = false;
            RepositoryConfig conf = new RepositoryConfig();
            if (dbIsOpened)
            {
                int v = Variant == 0 ? 7 : 9;
                conf.addParameter("FileName", $"БД {v} варианта");
            }
            else if (filePath != "")
            {
                conf.addParameter("FileName", filePath);
            }
            reposSaved?.Invoke(this, new FileEventArgs(conf));
            RefreshStatusLabel(0);
        }

        private void fileSaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = Variant == 0 ? "CSV файл (*.csv)|*.csv" : "Бинарный файл (*.bin)|*.bin";
            saveFileDialog.FileName = "";
            saveFileDialog.ShowDialog();
            RefreshStatusLabel(0);
        }

        private void fileSaveAs(object sender, CancelEventArgs e)
        {
            if (saveFileDialog.FileName != "")
            {
                filePath = saveFileDialog.FileName;
                fileChanged = false;
                RepositoryConfig conf = new RepositoryConfig();
                conf.addParameter("FileName", filePath);
                reposSaved?.Invoke(this, new FileEventArgs(conf));
            }
        }

        private void fileClose_Click(object sender, EventArgs e)
        {
            filePath = "";
            fileChanged = false;
            dbIsOpened = false;
            reposClosed?.Invoke();
            RefreshControls();
        }

        private void addRecord_Click(object sender, EventArgs e)
        {
            int pos = 0;
            if (dataGridView1.SelectedRows.Count == 1)
                pos = dataGridView1.SelectedRows[0].Index + 1;
            if (Variant == 0)
            {
                var form = new AddForm1();
                form.ShowDialog();
                if (!form.Canceled)
                {
                    fileChanged = true;
                    recordAdded?.Invoke(this, new AddEventArgs(form.Structure));
                    RefreshStatusLabel(0);
                }
            }
            else
            {
                var form = new AddForm2();
                form.ShowDialog();
                if (!form.Canceled)
                {
                    fileChanged = true;
                    recordAdded?.Invoke(this, new AddEventArgs(form.Structure));
                    RefreshStatusLabel(0);
                }
            }
        }

        private void editRecord_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                int pos = dataGridView1.SelectedRows[0].Index;
                var row = dataGridView1.SelectedRows[0];
                if (Variant == 0)
                {
                    var form = new AddForm1((string)row.Cells[0].Value, (string)row.Cells[1].Value, (DateTime)row.Cells[2].Value);
                    form.Text = "Изменить";
                    form.ShowDialog();
                    if (!form.Canceled)
                    {
                        fileChanged = true;
                        recordEdited?.Invoke(this, new EditEventArgs(form.Structure, pos));
                        RefreshStatusLabel(0);
                    }
                }
                else
                {
                    var form = new AddForm2((string)row.Cells[0].Value, (string)row.Cells[2].Value);
                    form.Text = "Изменить";
                    form.ShowDialog();
                    if (!form.Canceled)
                    {
                        fileChanged = true;
                        recordEdited?.Invoke(this, new EditEventArgs(form.Structure, pos));
                        RefreshStatusLabel(0);
                    }
                }
            }
        }

        private void deleteRecord_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 1)
            {
                fileChanged = true;
                recordDeleted?.Invoke(this, new DeleteEventArgs(dataGridView1.SelectedRows[0].Index));
                RefreshStatusLabel(0);
            }
        }

        private void run_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                updateLog("\nЗапуск интерпретатора...");
                compile?.Invoke(this, new CompileEventArgs(richTextBox2.Text));
            }
            else
            {
                try
                {
                    int a = int.Parse(textBox1.Text);
                    int b = int.Parse(textBox2.Text);
                    calculate?.Invoke(this, new CalculateEventArgs(a, b));
                }
                catch (FormatException)
                {
                    MessageBox.Show("Ошибка ввода", "Ошибка");
                }
            }
        }

        private void undo_Click(object sender, EventArgs e)
        {
            richTextBox2.Undo();
        }

        private void redo_Click(object sender, EventArgs e)
        {
            richTextBox2.Redo();
        }

        private void copy_Click(object sender, EventArgs e)
        {
            richTextBox2.Copy();
        }

        private void paste_Click(object sender, EventArgs e)
        {
            richTextBox2.Paste();
        }

        private void cut_Click(object sender, EventArgs e)
        {
            richTextBox2.Cut();
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            groupBox1.Location = new Point((groupBox1.Parent.Size.Width - groupBox1.Size.Width) / 2, (groupBox1.Parent.Size.Height - groupBox1.Size.Height) / 2);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            richTextBox1.ScrollToCaret();
            richTextBox1.Refresh();
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Variant = toolStripComboBox1.SelectedIndex;
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Variant = toolStripComboBox2.SelectedIndex;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshControls();
        }

        private void работаСоСтруктурамиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void языковыеКнострукцииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }

        private void низкоуровневыеКомандыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            RefreshStatusLabel(0);
        }

        private void richTextBox2_SelectionChanged(object sender, EventArgs e)
        {
            RefreshStatusLabel(1);
        }

        private void открытьИзБДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new DbForm();
            form.ShowDialog();
            if (!form.Canceled)
            {
                Variant = form.Variant;
                fileVariant = Variant;
                dbIsOpened = true;
                dbOpened?.Invoke(Variant);
                RefreshControls();
            }
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new HelpForm().ShowDialog();
        }
    }
}
