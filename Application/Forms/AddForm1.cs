﻿using System;
using System.Windows.Forms;

namespace Application
{
    public partial class AddForm1 : Form
    {
        public AddForm1()
        {
            InitializeComponent();
            Canceled = false;
        }

        public AddForm1(string adress, string accessMode, DateTime date)
        {
            InitializeComponent();
            Canceled = false;
            textBox1.Text = adress;
            textBox2.Text = accessMode;
            dateTimePicker1.Value = date;
        }

        public StructB Structure { get; private set; }
        public bool Canceled { get; private set; }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0 || textBox2.Text.Length == 0)
            {
                MessageBox.Show("Введены не все данные", "Ошибка");
            }
            else
            {
                Structure = new StructB(textBox1.Text, textBox2.Text, dateTimePicker1.Value);
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Canceled = true;
            this.Close();
        }
    }
}
