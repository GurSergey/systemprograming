﻿using System;
using System.Windows.Forms;

namespace Application
{
    public partial class AddForm2 : Form
    {
        public AddForm2()
        {
            InitializeComponent();
            Canceled = false;
        }

        public AddForm2(string login, string email)
        {
            InitializeComponent();
            Canceled = false;
            textBox1.Text = login;
            textBox3.Text = email;
        }

        public StructG Structure { get; private set; }
        public bool Canceled { get; private set; }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0 || textBox2.Text.Length == 0 || textBox3.Text.Length == 0)
            {
                MessageBox.Show("Введены не все данные", "Ошибка");
            }
            else
            {
                Structure = new StructG(textBox1.Text, textBox2.Text, textBox3.Text);
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Canceled = true;
            this.Close();
        }
    }
}
