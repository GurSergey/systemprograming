﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Application
{
    /// <summary>
    ///  Основной класс приложения 
    /// в нем происходит конфигурация всего
    /// приложения.
    /// При помощи Dependency injection
    /// классы привязываются друг к другу
    /// (большинство классов работают с абстракциями)
    /// </summary>
    internal class Application
    {
        /// <summary>
        /// Точка входа в приложение
        /// </summary>
        [STAThreadAttribute]
        public static void Main(string[] args)
        {
            //using (UserContext db = new UserContext())
            //{
            //    // создаем два объекта User
            //    User user1 = new User { Name = "Tom", Age = 33 };
            //    User user2 = new User { Name = "Sam", Age = 26 };

            //    // добавляем их в бд
            //    db.Users.Add(user1);
            //    db.Users.Add(user2);
            //    db.SaveChanges();
            //    Console.WriteLine("Объекты успешно сохранены");

            //    // получаем объекты из бд и выводим на консоль
            //    var users = db.Users;
            //    Console.WriteLine("Список объектов:");
            //    foreach (User u in users)
            //    {
            //        Console.WriteLine("{0}.{1} - {2}", u.Id, u.Name, u.Age);
            //    }
            //}

            IView view = new MainForm();
            //объявление всех штучек 7 варианта
            IRepository<StructB> repositoryB = new RepositoryFilesB<StructB>();
            IRepository<StructB> db_reposB = new RepositoryDB<StructB>();
            ILowLevelFunc lowLevelFuncB = new LowLevelFuncB();
            ICompiler compilerB = new CompilerB();
            Model<StructB> modelB = new Model<StructB>(repositoryB, db_reposB, lowLevelFuncB, compilerB);
            Presenter<StructB> presenterB = new Presenter<StructB>(view, modelB, 0);

            //объявление всех штучек 9 варианта
            IRepository<StructG> repositoryG = new RepositoryFilesG<StructG>();
            IRepository<StructG> db_reposG = new RepositoryDB<StructG>();
            ILowLevelFunc lowLevelFuncG = new LowLevelFuncG();
            ICompiler compilerG = new CompilerG();
            Model<StructG> modelG = new Model<StructG>(repositoryG, db_reposG, lowLevelFuncG, compilerG);
            Presenter<StructG> presenterG = new Presenter<StructG>(view, modelG, 1);

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.Run((MainForm)view);
            
            
        }
    }
}