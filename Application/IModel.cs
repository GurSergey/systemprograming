using System;

namespace Application
{
    /// <summary>
    /// Интерфейс модели
    /// </summary>
    /// <typeparam name="T">Тип значения с которым работает репозитоий модели</typeparam>
    public interface IModel<T>
    {   
        /// <summary>
        /// Инициализация репозитория модели
        /// </summary>
        /// <param name="config">Словарь-конфигуратор</param>
        void init(RepositoryConfig config);
        /// <summary>
        /// Сохранение записей в реозитории
        /// </summary>
        /// <param name="config">Словарь-конфигуратор</param>
        void save(RepositoryConfig config);
        /// <summary>
        /// Закрытиеь репозитория 
        /// </summary>
        void close();
        /// <summary>
        /// Добавление записи
        /// </summary>
        /// <param name="rec">Добавляемая запись</param>
        void addRecord(IStruct rec);
        /// <summary>
        /// Редактирование записи
        /// </summary>
        /// <param name="rec">Запись</param>
        /// <param name="index"> Номер-id записи</param>
        void editRecord(IStruct rec, int 
        index);
        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="index"></param>
        void deleteRecord(int index);
        /// <summary>
        /// Вызов метода интепретации
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        string compile(string code);
        /// <summary>
        /// Вызов функции подсчета
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        ValueType calculate(ValueType a, ValueType b, out string error);
        /// <summary>
        /// Получить все записи из репозитория
        /// </summary>
        /// <returns></returns>
        Record<T>[] getAllRecords();
        /// <summary>
        /// Использование базы в качестве репозитория
        /// </summary>
        bool UseDB { get; set; }
    }
}