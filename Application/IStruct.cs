﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    /// <summary>
    /// Интерфейс структуры для сохранения в репозитории
    /// </summary>
    public interface IStruct
    {
        /// <summary>
        /// Id записи
        /// </summary>
        int Id { get; set; }
    }
}
