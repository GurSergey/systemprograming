using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace Application
{
    /// <summary>
    /// Интепретор 9 варианта
    /// </summary>
    public class CompilerG : ICompiler
    {
        private const string ERROR_INTERPRETAION = "Ошибка исполнения  ";
        private const string RESULT_IF = "Выполнится условный оператор  ";
        private const string RESULT_ELSE = "Выполнится блок else условного оператора  ";
        private const string RESULT_NOT = "Не выполнится условный оператор ";

        public static readonly string[] typeOfVars = {"int", "double", "bool"};
        private List<Variable> vars = new List<Variable>();
        private string message = "";
        /// <summary>
        /// Класс переменной 
        /// </summary>
        private class Variable
        {
            public string Name => name;

            public string Type => type;

            public object Value { get; set; }

            private string name;
            private string type;
            
            public Variable(string Name, string type, object Value)
            {
                this.name = Name;
                this.type = type;
                this.Value = Value;
            }
        }
        /// <summary>
        /// Класс общий предок синтаксического элемента
        /// </summary>
        public abstract class SyntaxElement
        {
            public static string pattern = "";
            protected string code;
            public abstract void execute();
            protected CompilerG comp;
            public SyntaxElement(CompilerG compiler,  string code)
            {
                this.comp = compiler;
                this.code = code;
                
            }

        }
        /// <summary>
        /// проверить создана ли переменная
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
       private bool checkCreatedVariable(string name)
        {
            foreach (var vari in vars)
            {
                if(name == vari.Name)
                    return true;
            }

            return false;
        }
        /// <summary>
        /// Получить переменную по имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private Variable getByName(string name)
        {
            foreach (var vari in vars)
            {
                if(name == vari.Name)
                    return vari;
            }

            return null;
        }
        /// <summary>
        /// Класс оператор логического типа
        /// </summary>
        private class BooleanOperator : SyntaxElement
        {
            private SyntaxElement elementLeft;
            private SyntaxElement elementRight;

            public static string pattern =
                @"^\s{0,}([0-9a-zA-Z\)\(\+\-\/\*]*)\s{0,}(==|!=|>=|<=|<|>)\s{0,}([0-9a-zA-Z\)\(\+\-\/\*]*)\s{0,}";
            
            public bool result;
            public override void execute()
            {
                Regex regex = new Regex(BooleanOperator.pattern);
                var match = regex.Match(comp.getOneLineInstruction(this.code));
                Expression expression1 = new Expression(comp, match.Groups[1].ToString());
                Expression expression2 = new Expression(comp, match.Groups[3].ToString());
                expression1.execute();
                expression2.execute();
                switch (match.Groups[2].ToString())
                {
                    case "==":
                        result = (double) expression1.result == (double) expression2.result;
                        break;
                    case "!=":
                        result = (double) expression1.result != (double) expression2.result;
                        break;
                    case ">=":
                        result = (double) expression1.result >= (double) expression2.result;
                        break;
                    case "<=":
                        result = (double) expression1.result <= (double) expression2.result;
                        break;
                    case "<":
                        result = (double) expression1.result < (double) expression2.result;
                        break;
                    case ">":
                        result = (double) expression1.result > (double) expression2.result;
                        break;
                }
            }

            public BooleanOperator(CompilerG compiler,  string code) : base(compiler,  code)
            {
            }
        }
        /// <summary>
        /// Класс объявления и присвоения
        /// </summary>
        private class AnnouncementAndAssignmentVar : SyntaxElement
        {
            public static string pattern =
                @"\s{0,}([a-zA-Z][0-9a-zA-Z]*)\s{0,}(=|\+=|\-=|\*=|\/=)\s{0,}([0-9a-zA-z\-\+\*\/\s\(\)]*)\s{0,};";

            static AnnouncementAndAssignmentVar()
            {
                string buf = "";
                for (int i = 0; i < typeOfVars.Length; i++)
                {
                    buf += typeOfVars[i] + (i != (typeOfVars.Length - 1) ? "|" : "");
                }

                AnnouncementAndAssignmentVar.pattern = @"^\s{0,}(" + buf + @")\s{0,}" + pattern;
            }
            public override void execute()
            {
                Regex regex = new Regex(AnnouncementAndAssignmentVar.pattern);
                var match = regex.Match(comp.getOneLineInstruction(this.code));
                if (comp.checkCreatedVariable(match.Groups[2].ToString()) == true)
                {
                    throw new Exception("Уже существет переменая с именем " + match.Groups[2].ToString()); //совпало имя 
                }

                Expression expression = new Expression(comp,  match.Groups[4].ToString());
                expression.execute();

                object result = null;
                switch (match.Groups[1].ToString())
                {
                    case "int":
                        result = (int) expression.result;
                        break;
                    case "double":
                        result = expression.result;
                        break;
                }

                switch (match.Groups[3].ToString())
                {
                    case "=":
                        comp.vars.Add(new Variable(regex.Match(this.code).Groups[2].ToString(),
                            regex.Match(this.code).Groups[1].ToString(), result));
                        break;
                    default:
                        throw new Exception("Неизвестная операция");
                        break;

                }
            }

            public AnnouncementAndAssignmentVar(CompilerG compiler,  string code) : base(compiler,
                 code)
            {
            }
        }
        /// <summary>
        /// Класс Создания переменной
        /// </summary>

        private class AnnouncementVar : SyntaxElement
        {
            public static string pattern = @"([a-zA-Z][0-9a-zA-Z]*)\s{0,};";

            static AnnouncementVar()
            {
                string buf = "";
                for (int i = 0; i < typeOfVars.Length; i++)
                {
                    buf += typeOfVars[i] + (i != (typeOfVars.Length - 1) ? "|" : "");
                }

                AnnouncementVar.pattern = @"^\s{0,}(" + buf + @")\s{0,}" + pattern;
            }

            public override void execute()
            {
                Regex regex = new Regex(AnnouncementVar.pattern);
                var match = regex.Match(comp.getOneLineInstruction(this.code));
                if (comp.checkCreatedVariable(match.Groups[2].ToString()) == true)
                {
                    throw new Exception(); //совпало имя 
                }
                comp.vars.Add(new Variable(match.Groups[2].ToString(),
                    match.Groups[1].ToString(), null));
            }

            public AnnouncementVar(CompilerG compiler,  string code) : base(compiler,  code)
            {
            }
        }
        /// <summary>
        /// Оператор присовения
        /// </summary>
        private class AssignmentOperator : SyntaxElement
        {
            public static string pattern =
                @"^\s{0,}([a-zA-Z][0-9a-zA-Z]*)\s{0,}(=|\+=|\-=|\*=|\/=)\s{0,}([0-9a-zA-z\-\+\*\/\s\(\)]*)\s{0,};";

            private SyntaxElement variable;


            public override void execute()
            {
                Regex regex = new Regex(AssignmentOperator.pattern);
                var match = regex.Match(comp.getOneLineInstruction(this.code));
                
                if ( !comp.checkCreatedVariable(match.Groups[1].ToString()))
                    throw new Exception("Не найдена переменная для присваивания");
                Expression expression = new Expression(comp, match.Groups[3].ToString());
                expression.execute();
                var variab = comp.getByName( match.Groups[1].ToString());

                object result = null;
                switch (variab.Type)
                {
                    case "int":
                        result = (int) expression.result;
                        break;
                    case "double":
                        result = expression.result;
                        break;
                    default:
                        throw new Exception("Неизвестный тип");
                        break;
                }
                    
                switch (match.Groups[2].ToString())
                {
                    case "=":
                        variab.Value = result;
                        break;
                    default:
                        throw new Exception("Неизвестная операция");
                        break;

                }
            }

            public AssignmentOperator(CompilerG compiler,  string code) : base(compiler,  code)
            {
            }

        }
        /// <summary>
        /// Пустая инструкция
        /// </summary>

        private class EmptyInstruction : SyntaxElement
        {
            public static string pattern = @"^\s{0,}";

            public override void execute()
            {
            }

            public EmptyInstruction(CompilerG compiler,  string code) : base(compiler,  code)
            {
            }
        }
        /// <summary>
        ///  Класс Выражения
        /// </summary>
        private class Expression : SyntaxElement
        {
            public static string pattern = @"^\s{0,}([0-9a-zA-Z\s\+\-\*\/\(\)]*)\s{0,}";
            private static string patternNameVar = @"([a-zA-Z][0-9a-zA-Z]*)";
            public double result;

            public override void execute()
            {
                string replacedCode = this.code;
                Regex regexVar = new Regex(patternNameVar);
                var matches = regexVar.Matches(replacedCode);
                
                foreach (var match in matches)
                { 
                    var gVar = comp.getByName(match.ToString());
                    if(gVar!=null)
                    replacedCode = replacedCode.Replace(match.ToString(), gVar.Value.ToString());
                   
                }

                result = comp.calc(replacedCode);
            }

            public Expression
                (CompilerG compiler,  string code) : base(compiler,  code)
            {
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instruction"></param>
        /// <returns></returns>
        private string getOneLineInstruction(string instruction)
        {
            return instruction.Replace(Environment.NewLine, "");
        }
        /// <summary>
        /// Класс содержащий одну инструкцию
        /// </summary>
        private class OneInstruction
        {
            private String instruction;

            public string Instruction => instruction;

            private bool isBlockInstruction = false;
            public OneInstruction( string inst)
            {
                
                instruction = inst;
            }
        }
        /// <summary>
        /// Класс читатель инструкций
        /// </summary>
        private class ReaderInstruction
        {
            private string startBlock = "{";
            private string endBlock = "}";
            private string currentInstruction = "";
            private string type = "";


            private string[] BlockInstructionKeywords = {"if"};
            private string[] addSpaceStr = {"if", "else", "{", "}", ";"};


            public OneInstruction[] readLine(String code)
            {
                foreach (var str in addSpaceStr)
                {
                    code = code.Replace(str, " " + str + " ");
                }

                List<OneInstruction> instructions = new List<OneInstruction>();
                string[] lines = code.Split(
                    new[] {"\n"},
                    StringSplitOptions.None
                );
                int numLine = 0;
                int countBlock = 0;
                bool expectedElseBlock = false;
                bool isContainElse = false;
                bool BlockInstruction = false;
                foreach (var line in lines)
                {
                    numLine++;
                    string[] words = line.Split(' ');
                    foreach (var word in words)
                    {
                        if (word == "")
                            continue;
                        currentInstruction += " " + word;

                        if ((BlockInstruction == false) && (word.Contains(";")))
                        {
                            instructions.Add(new OneInstruction( currentInstruction));
                            currentInstruction = "";
                        }

                        if (BlockInstruction == false)
                        {
                            foreach (string one in BlockInstructionKeywords)
                            {
                                if (word.Contains(one))
                                {
                                    BlockInstruction = true;
                                    this.type = one;
                                    break;
                                }
                            }
                        }

                        if (BlockInstruction)
                        {
                            if (word == startBlock)
                            {
                                countBlock++;
                            }

                            if (word == endBlock)
                            {
                                countBlock--;
                            }

                            switch (this.type)
                            {
                                case "if":
                                    if (expectedElseBlock == true)
                                    {
                                        if ((word == "else") && (countBlock == 0))
                                        {
                                            isContainElse = true;
                                        }
                                        else
                                        {
                                            currentInstruction = currentInstruction.Substring(0,
                                                currentInstruction.Length - word.Length);
                                            instructions.Add(new OneInstruction( currentInstruction));
                                            BlockInstruction = false;
                                            isContainElse = false;
                                            currentInstruction = "" + word;
                                        }

                                        expectedElseBlock = false;
                                    }

                                    if ((word == endBlock) && (expectedElseBlock == false) && (countBlock == 0))
                                    {
                                        expectedElseBlock = true;
                                    }

                                    if (isContainElse)
                                    {
                                        if ((word == endBlock) && (countBlock == 0))
                                        {
                                            instructions.Add(new OneInstruction( currentInstruction));
                                            currentInstruction = "";
                                            BlockInstruction = false;
                                            expectedElseBlock = false;
                                            isContainElse = false;
                                        }
                                    }

                                    break;
                                default:
                                    throw new Exception("Неизвестный блок");
                                    break;
                            }
                        }
                    }

                }
                instructions.Add(new OneInstruction( currentInstruction));
                return instructions.ToArray();
            }
        }
        /// <summary>
        /// Класс Условное выражение
        /// </summary>
        public class IfExpression : SyntaxElement
        {
            private SyntaxElement expr;
            private SyntaxElement ifBlock;
            private SyntaxElement elseBlock;

            public static string pattern =
                @"^\s{0,}if\s{0,}\(([^\(\)]*)\)\s{0,}{([^{}]*)}\s{0,}(else\s{0,} {([^{})]*)})?";
            public override void execute()
            {
                Regex regex = new Regex(pattern);
                var match = regex.Match(comp.getOneLineInstruction(code));

                BooleanOperator booleanOperator = new BooleanOperator(this.comp,  match.Groups[1].ToString());
                booleanOperator.execute();
                if (booleanOperator.result)
                {
                    //ContextVar newContext = new ContextVar(comp.currentContext, comp.currentContext.NestedLevel + 1);
                    
                    comp.compileMultilineBlock(match.Groups[2].ToString());
                    comp.message += CompilerG.RESULT_IF + " if ("+match.Groups[1].ToString()+")"+Environment.NewLine;

                }
                else
                {
                    if (match.Groups[3].ToString().Contains("else"))
                    {
                        comp.message += CompilerG.RESULT_ELSE + " if ("+match.Groups[1].ToString()+")"+Environment.NewLine;
                        comp.compileMultilineBlock(match.Groups[4].ToString());
                    }
                    else
                    {
                        comp.message += CompilerG.RESULT_NOT + " if ("+match.Groups[1].ToString()+")"+Environment.NewLine;
                    }
                }
            }
            public IfExpression(CompilerG compiler,  string code) : base(compiler,  code)
            {
            }
        }
        
        /// <summary>
        /// Метод вычисляющий значение выражения 
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
       private double calc(string exp)
        {
            try
            {
                var object1 = new DataTable().Compute(exp, "");
                return Convert.ToDouble(object1);
            }
            catch (Exception e)
            {
                
                throw new Exception("Ошибка вычисления выражения " +exp);
            }
            
        }
        /// <summary>
        /// Метод выполняющий многострочный блок кода
        /// </summary>
        /// <param name="code"></param>
        /// <exception cref="Exception"></exception>
       public void compileMultilineBlock(string code)
        {
            ReaderInstruction reader = new ReaderInstruction();
            OneInstruction[] instructions = reader.readLine(code);
            //регистрация всех типов синтаксических конструкций
            Type[] syntaxElems =
            {
                typeof(AnnouncementAndAssignmentVar),
                typeof(AnnouncementVar), typeof(AssignmentOperator),
                typeof(IfExpression), typeof(EmptyInstruction)

            };
            foreach (var instruction in instructions)
            {
                bool flag = false;
                foreach (var syntaxElem in syntaxElems)
                {
                    var pattern = (string) syntaxElem.GetField("pattern").GetValue(null);
                    Regex regex = new Regex(pattern);
                    string oneLineInstr = getOneLineInstruction(instruction.Instruction);
                    if (regex.Match(getOneLineInstruction(oneLineInstr)).Success)
                    {
                        SyntaxElement instance =
                            (SyntaxElement) Activator.CreateInstance(syntaxElem,
                                new object[] {this,  oneLineInstr});
                        instance.execute();
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                    throw new Exception("Инструкиця не может быть исполнена:"+instruction.Instruction);
            }
        }

        public string compile(string code)
        {
            vars = new List<Variable>();
            message = "";
            try
            {
                compileMultilineBlock(code);
            }
            catch (Exception e)
            {
                return CompilerG.ERROR_INTERPRETAION + ":\n" + e.Message;
            }
            
            return this.message;
        }
    }
}
