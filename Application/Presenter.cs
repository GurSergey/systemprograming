using System;
using System.Data;
using System.Windows.Forms;
using ApplicationException = Application.Exceptions.ApplicationException;

namespace Application
{
    /// <summary>
    ///  Класс-Презентатор, реализующий Prenster в программной модели MVP
    /// </summary>
    /// <typeparam name="T">
    /// Тип структуры, с которой работает данный эземляр презентатора и как следствие модель
    /// </typeparam>
    public class Presenter<T>
    {
        private IView view;
        private IModel<T> model;
        private int variant;
        DataTable table;
        string[,] columnNames = new string[2, 3] { { "Адрес", "Режим доступа", "Дата доступа" }, { "Логин", "Хэш пароля", "Электронная почта" } };
        string[,] fieldNames = new string[2, 3] { { "Adress", "AccessMode", "AccessDate" }, { "Login", "HashCode", "EMail" } };
        Type[,] fieldTypes = new Type[2, 3] { { typeof(string), typeof(string), typeof(DateTime) }, { typeof(string), typeof(string), typeof(string) } };
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="view"> Представление(форма, веб-интерфейс и т.д.)</param>
        /// <param name="model">Модель</param>
        /// <param name="variant">Номер Варианта (0 или 1)</param>
        public Presenter(IView view, IModel<T> model, int variant)
        {
            this.view = view;
            this.model = model;
            this.variant = variant;

            view.reposCreated += onReposCreate;
            view.reposOpened += onReposOpen;
            view.reposSaved += onReposSave;
            view.reposClosed += onReposClose;
            view.recordAdded += onRecordAdd;
            view.recordEdited += onRecordEdit;
            view.recordDeleted += onRecordDelete;
            view.compile += onCompile;
            view.calculate += onCalculate;
            view.dbOpened += onDbOpen;
        }
        /// <summary>
        /// Событие создания нового репозитория
        /// пришедшего из модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> Аргументы события</param>
        private void onReposCreate(object sender, FileEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    model.UseDB = false;
                    model.init(e.Config);
                    table = new DataTable();
                    DataColumn[] cols = new DataColumn[3];
                    for (int i = 0; i < 3; i++)
                    {
                        cols[i] = new DataColumn(columnNames[variant, i], fieldTypes[variant, i]);
                    }

                    table.Columns.AddRange(cols);
                    view.updateStructs(table);
                    view.updateLog($"Создан файл {e.Config.getParameter("FileName")}");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие открытия репозитория
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> Аргументы события</param>
        private void onReposOpen(object sender, FileEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    model.UseDB = false;
                    model.init(e.Config);
                    table = new DataTable();
                    DataColumn[] cols = new DataColumn[3];
                    for (int i = 0; i < 3; i++)
                    {
                        cols[i] = new DataColumn(columnNames[variant, i], fieldTypes[variant, i]);
                    }

                    table.Columns.AddRange(cols);
                    Record<T>[] records = model.getAllRecords();
                    for (int i = 0; i < records.Length; i++)
                    {
                        table.Rows.Add(table.NewRow());
                        for (int j = 0; j < 3; j++)
                        {
                            T struc = records[i].RecordStruct;
                            table.Rows[i][j] = struc.GetType().GetProperty(fieldNames[variant, j]).GetValue(struc);
                        }
                    }

                    view.updateStructs(table);
                    view.updateLog($"Открыт файл {e.Config.getParameter("FileName")}");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// При сохранении репозитория
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> Аргументы события</param>
        private void onReposSave(object sender, FileEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    model.save(e.Config);
                    view.updateLog($"Файл сохранён в {e.Config.getParameter("FileName")}");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие закрытия репозитория
        /// </summary>

        private void onReposClose()
        {
            try
            {
                if (view.Variant == variant)
                {
                    model.close();
                    table = new DataTable();
                    view.updateStructs(table);
                    view.updateLog($"Файл закрыт");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие добавления записи
        /// </summary>
        private void onRecordAdd(object sender, AddEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    model.addRecord(e.Structure);
                    table.Rows.Add(table.NewRow());
                    for (int i = 0; i < 3; i++)
                    {
                        table.Rows[table.Rows.Count - 1][i] = e.Structure.GetType().GetProperty(fieldNames[variant, i])
                            .GetValue(e.Structure);
                    }

                    view.updateLog("Добавлена новая запись");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие редактирования записи
        /// </summary>
        private void onRecordEdit(object sender, EditEventArgs e)
        {
            try
            {

                if (view.Variant == variant)
                {
                    model.editRecord(e.Structure, e.Pos);
                    for (int i = 0; i < 3; i++)
                    {
                        table.Rows[e.Pos][i] = e.Structure.GetType().GetProperty(fieldNames[variant, i])
                            .GetValue(e.Structure);
                    }

                    view.updateLog($"Запись {e.Pos} изменена");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие удаления репозитория
        /// </summary>
        private void onRecordDelete(object sender, DeleteEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    model.deleteRecord(e.Pos);
                    table.Rows.RemoveAt(e.Pos);
                    view.updateLog($"Запись {e.Pos} удалена");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие начало интепретации выражения
        /// </summary>
       
        private void onCompile(object sender, CompileEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    view.updateLog(model.compile(e.Code));
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие подсчет низкоуровневой функции
        /// </summary>
        private void onCalculate(object sender, CalculateEventArgs e)
        {
            try
            {
                if (view.Variant == variant)
                {
                    string error;
                    view.updateCalculate(model.calculate(e.A, e.B, out error).ToString());
                    if (error.Length > 0)
                        view.updateLog(error);
                    else
                        view.updateLog($"Выполнено {(variant == 0 ? "деление" : "умножение")}");
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" + exc.GetMessage());
            }
        }
        /// <summary>
        /// Событие открытия базы данных
        /// </summary>
        ///  /// <param name="v"> Номер варианта</param>
        private void onDbOpen(int v)
        {
            try
            {
                if (variant == v)
                {
                    model.UseDB = true;
                    Cursor.Current = Cursors.WaitCursor;
                    model.init(new RepositoryConfig());
                    table = new DataTable();
                    DataColumn[] cols = new DataColumn[3];
                    for (int i = 0; i < 3; i++)
                    {
                        cols[i] = new DataColumn(columnNames[variant, i], fieldTypes[variant, i]);
                    }

                    table.Columns.AddRange(cols);
                    Record<T>[] records = model.getAllRecords();
                    for (int i = 0; i < records.Length; i++)
                    {
                        table.Rows.Add(table.NewRow());
                        for (int j = 0; j < 3; j++)
                        {
                            T struc = records[i].RecordStruct;
                            table.Rows[i][j] = struc.GetType().GetProperty(fieldNames[variant, j]).GetValue(struc);
                        }
                    }

                    view.updateStructs(table);
                    int currentVariant = variant == 0 ? 7 : 9;
                    view.updateLog($"Открыта база данных {currentVariant} варианта");
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (ApplicationException exc)
            {
                view.updateLog("В модели произошла ошибка:" +exc.GetMessage());
            }
        }
    }
}