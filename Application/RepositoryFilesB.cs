using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Application
{
    /// <summary>
    /// Репозиторий для работы с файлами вариант №7
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RepositoryFilesB<T> : IRepository<T>
    {
        private const string paramFilePath = "FileName";
        private string filePath;
        private List<Record<T>> list;

        public void init(RepositoryConfig config)
        {
            list = new List<Record<T>>();
            filePath = config.getParameter(paramFilePath);

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                var sr = new StreamReader(fs);
                string content = sr.ReadToEnd();
                list = StringToList(content);
                sr.Close();
            }
        }

        public void save(RepositoryConfig config)
        {
            filePath = config.getParameter(paramFilePath);

            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                var sw = new StreamWriter(fs);
                sw.Write(ListToString(list));
                sw.Close();
            }
        }

        public void close()
        {
            filePath = "";
            if (list != null)
                list.Clear();
        }

        public Record<T>[] GetAllRecords()
        {
            return list.ToArray();
        }

        public void addRecord(Record<T> rec)
        {
            list.Add(rec);
        }

        public void editRecord(Record<T> rec, int index)
        {
            list[index] = rec;
        }

        public void deleteRecord(int index)
        {
            list.RemoveAt(index);
        }

        private string ListToString(List<Record<T>> list)
        {
            if (list.Count == 0)
                return "";
            StringBuilder result = new StringBuilder();
            string[] fieldNames = Record<T>.StructFieldNames;
            for (int i = 0; i < list.Count; i++)
            {
                T struc = list[i].RecordStruct;
                for (int j = 0; j < fieldNames.Length; j++)
                {
                    var b = struc.GetType().GetProperties();
                    var a = struc.GetType().GetProperty(fieldNames[j]);
                    string rec = a.GetValue(struc).ToString();
                    rec = rec.Replace("\"", "\"\"");
                    rec = rec.Replace(",", "\",\"");
                    rec = rec.Replace("\n", "\"\n\"");
                    result.Append(rec);
                    result.Append(',');
                }
                result.Remove(result.Length - 1, 1);
                result.Append('\n');
            }
            result.Remove(result.Length - 1, 1);
            return result.ToString();
        }

        private List<Record<T>> StringToList(string str)
        {
            List<Record<T>> result = new List<Record<T>>();
            if (str.Length == 0)
                return result;
            List<string> values = new List<string>();
            List<string[]> records = new List<string[]>();
            StringBuilder builder = new StringBuilder();
            bool skipCheck = false;
            bool ignore = false;
            int i = 0;
            while (i < str.Length)
            {
                if (ignore)
                    ignore = false;
                else
                {
                    if (skipCheck)
                    {
                        builder.Append(str[i]);
                        skipCheck = false;
                        if (str[i] != '"')
                            ignore = true;
                    }
                    else
                        switch (str[i])
                        {
                            case '"':
                                {
                                    skipCheck = true;
                                    break;
                                }
                            case ',':
                                {
                                    values.Add(builder.ToString());
                                    builder.Clear();
                                    break;
                                }
                            case '\n':
                                {
                                    values.Add(builder.ToString());
                                    builder.Clear();
                                    records.Add(values.ToArray());
                                    values.Clear();
                                    break;
                                }
                            default:
                                {
                                    builder.Append(str[i]);
                                    break;
                                }
                        }
                }
                i++;
            }
            if (builder.Length > 0)
                values.Add(builder.ToString());
            if (values.Count > 0)
                records.Add(values.ToArray());

            for (i = 0; i < records.Count; i++)
            {
                T struc = (T)Activator.CreateInstance(typeof(T), records[i]);
                result.Add(new Record<T>(struc));
            }

            return result;
        }
    }
}