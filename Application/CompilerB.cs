using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Application
{
    public class CompilerB:ICompiler
    {
        bool sucsess = false;
        Regex[] instructions = new Regex[]
        {
            new Regex(@"^\s*(int|double|bool)\s+([A-Za-z_]\w*)\s*=\s*([^;]+);\s*"),  //объявление переменной
            new Regex(@"^\s*([A-Za-z_]\w*)\s*(\=|\+\=|\-\=|\*\=|\/\=|\+\+|\-\-)\s*([^;]*);\s*"),  //присвоение переменной
            new Regex(@"^\s*while\s*\(\s*((?>.|\s)+)\s*\)\s*{\s*((?>.|\s)+)}\s*")   //цикл while
        };
        Regex expressionRegex = new Regex(@"\s*((?>.|\s)+);");
        static readonly List<string> reservedNames = new List<string> { "bool", "int", "double", "while", "true", "false" };
        List<Variable> variables;
        /// <summary>
        /// Главный метод запускающий интерпетацию
        /// </summary>
        /// <param name="code"></param>
        /// <returns>Строка-статус</returns>
        public string compile(string code)
        {
            variables = new List<Variable>();
            string a = Calculate("");
            int initialLength = code.Length;
            byte whileCount = 0;
            int regexIndex;
            StringBuilder codeBuffer = new StringBuilder(code);
            while (codeBuffer.Length > 0)
            {
                Match match = FindMatch(codeBuffer.ToString(), out regexIndex);
                if (regexIndex == -1)
                    return "Синтаксическая ошибка";
                else if (regexIndex == 2)
                {
                    whileCount++;
                    if (whileCount > 1)
                        return "Не поддерживается обработка более чем одной конструкции while";
                }
                codeBuffer.Remove(0, match.Value.Length);
                try
                {
                    Execute(match, regexIndex);
                }
                catch (ExecuteException e)
                {
                    return e.Message;
                }
            }
            if (whileCount == 0)
                return "Не найдена конструкция while";
            else
                return sucsess ? "Цикл выполнится хотя бы один раз" : "Цикл не выполнится ни разу";
        }
        /// <summary>
        /// Метод ищет совпадения с регулярными выражениями
        /// </summary>
        /// <param name="code"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        Match FindMatch(string code, out int index)
        {
            index = 0;
            Match result;
            do
            {
                result = instructions[index].Match(code);
                index++;
            }
            while (index < instructions.Length && !result.Success);
            if (result.Success)
                index--;
            else
                index = -1;
            return result;
        }
        /// <summary>
        /// Метод исполняет инструкцию
        /// </summary>
        /// <param name="instruction"></param>
        /// <param name="regexIndex"></param>
        /// <exception cref="ExecuteException"></exception>
        private void Execute(Match instruction, int regexIndex)
        {
            switch (regexIndex)
            {
                case 0:
                    {
                        if (variables.Any(x => x.Name == instruction.Groups[2].Value))
                            throw new ExecuteException($"Ошибка: переменная {instruction.Groups[2].Value} уже была объявлена");
                        else
                        {
                            Variable v = new Variable(instruction.Groups[1].Value, instruction.Groups[2].Value);
                            variables.Add(v);
                            if (instruction.Groups.Count > 3)
                            {
                                string val = Calculate(instruction.Groups[3].Value);
                                if (v.Type == "bool" && !(val == "True" || val == "False"))
                                    throw new ExecuteException("Попытка присвоения булевой переменной числового значения");
                                if (v.Type != "bool" && (val == "True" || val == "False"))
                                    throw new ExecuteException("Попытка присвоения числовой переменной булевого значения");
                                v.Value = val;
                            }
                        }
                        break;
                    }
                case 1:
                    {
                        Variable v = variables.Find(x => x.Name == instruction.Groups[1].Value);
                        if (v == null)
                            throw new ExecuteException($"Переменная {instruction.Groups[1].Value} не объявлена");
                        string operation = instruction.Groups[2].Value;
                        if (!(operation == "++" || operation == "--") && instruction.Groups.Count < 4)
                            throw new ExecuteException("Отсутствует правая часть выражения");
                        string val = Calculate(instruction.Groups[3].Value);
                        if (val == "")
                            throw new ExecuteException("Отсутствует правая часть выражения");
                        if (v.Type == "bool")
                        {
                            if (!(val == "True" || val == "False"))
                                throw new ExecuteException("Попытка присвоения булевой переменной числового значения");
                            if (operation != "=")
                                throw new ExecuteException("Недопустимая операция для булевой переменной");
                        }
                        else if (val == "True" || val == "False")
                            throw new ExecuteException("Попытка присвоения числовой переменной булевого значения");
                        switch (operation)
                        {
                            case "++":
                                v.Value = (Convert.ToDouble(v.Value) + 1).ToString();
                                break;
                            case "--":
                                v.Value = (Convert.ToDouble(v.Value) - 1).ToString();
                                break;
                            case "=":
                                v.Value = val;
                                break;
                            case "+=":
                                v.Value = (Convert.ToDouble(v.Value) + Convert.ToDouble(val)).ToString();
                                break;
                            case "-=":
                                v.Value = (Convert.ToDouble(v.Value) - Convert.ToDouble(val)).ToString();
                                break;
                            case "*=":
                                v.Value = (Convert.ToDouble(v.Value) * Convert.ToDouble(val)).ToString();
                                break;
                            case "/=":
                                double temp = Convert.ToDouble(v.Value) / Convert.ToDouble(val);
                                if (v.Type == "int")
                                    v.Value = ((int)temp).ToString();
                                else
                                    v.Value = temp.ToString();
                                break;
                        }
                        break;
                    }
                case 2:
                    {
                        sucsess = Calculate(instruction.Groups[1].Value) == "True";

                        break;
                    }
            }
        }
        /// <summary>
        /// Метод вычисляет значение выражения
        /// </summary>
        /// <param name="expression">выражение</param>
        /// <returns>во</returns>
        /// <exception cref="ExecuteException"></exception>
        private string Calculate(string expression)
        {
            DataTable table = new DataTable();
            Regex varRegex = new Regex(@"[A-Za-z_]\w*");
            var matches = varRegex.Matches(expression);
            foreach (Match match in matches)
            {
                if (reservedNames.Contains(match.Value))
                    continue;
                Variable v = variables.Find(x => x.Name == match.Value);
                if (v == null)
                    throw new ExecuteException($"Переменная {match.Value} не найдена");
                if (v.Value == "")
                    throw new ExecuteException($"Переменной {match.Value} не присвоено значение");
                expression = expression.Replace(match.Value, v.Value);  //я знаю, что 2one будет заменено на 21, но я устал
            }
            expression = expression.Replace("||", " or ");
            expression = expression.Replace("&&", " and ");
            expression = expression.Replace("!", " not ");
            expression = expression.Replace("==", "=");
            expression = expression.Replace("++", " +1 ");  //я знаю, что это не будет учитывать приоритет последующей операции, но я устал
            expression = expression.Replace("--", " -1 ");
            string result;
            try
            {
                result = table.Compute(expression, "").ToString();
            }
            catch (Exception e)
            {
                throw new ExecuteException(e.Message);
            }
            return result;
        }
        /// <summary>
        /// Класс ошибки исполнения
        /// </summary>
        class ExecuteException : Exception
        {
            public ExecuteException(string msg)
            {
                Message = msg;
            }

            public override string Message { get; }
        }
        /// <summary>
        /// Класс перемнной
        /// </summary>
        class Variable
        {
            public Variable(string type, string name)
            {
                if (reservedNames.Contains(name))
                    throw new ExecuteException("Некорректное имя переменной");
                Type = type;
                Name = name;
            }
            public string Type { get; }
            public string Name { get; }
            public string Value { get; set; }
        }
    }
}