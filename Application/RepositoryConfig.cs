using System;
using System.Collections.Generic;
using System.Security;

namespace Application
{
    /// <summary>
    /// Класс-словарь настроек репозитория
    /// </summary>
    public class RepositoryConfig
    {
        /// <summary>
        /// Словарь Строка(имя параметра)-Строка(значение)
        /// </summary>
        private Dictionary<String, String> parameters;
        /// <summary>
        /// Конструктор
        /// </summary>
        public RepositoryConfig()
        {
            parameters = new Dictionary<string, string>();
        }
        /// <summary>
        /// Добавить параметр
        /// </summary>
        /// <param name="key"> Ключ параметра</param>
        /// <param name="param">Значение параметра</param>
        public void addParameter(string key, string param)
        {
            parameters.Add(key, param);
        }
        /// <summary>
        /// Возвращает значение параметра по имени параметра
        /// </summary>
        /// <param name="param">Имя параметра</param>
        /// <returns>Значение</returns>
        public String getParameter(string param)
        {
            string val;
            parameters.TryGetValue(param, out val);
            return val;
        }
    }
}