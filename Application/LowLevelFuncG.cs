using System;
using System.Reflection;

namespace Application
{
    /// <summary>
    /// Класс предоставляющий возможность работать
    /// с библеотекой dll Варианта №9
    /// </summary>
    public class LowLevelFuncG:ILowLevelFunc
    {
        public static string nameDll = "lowLevelFuncG";
        public static string nameClass = "LowLevelFunc";
        public static string nameFunction = "Function";
        /// <summary>
        /// Подсчет значения
        /// </summary>
        /// <param name="a">Первый параметр</param>
        /// <param name="b">Второй параметр</param>
        /// <param name="error">Текст содержащий ошибку</param>
        /// <returns>Значение вычесленной функции</returns>
        public ValueType Function(ValueType a, ValueType b, out string error)
        {
            Assembly assembly = Assembly.Load(nameDll);
            Object obj = assembly.CreateInstance(nameClass);
            Type type = assembly.GetType(nameClass);
            MethodInfo method = type.GetMethod(nameFunction);
            
            Object[] args = new object[2];
            args[0] = a; 
            args[1] = b;
            error = "";
            try
            {
                return (int) method.Invoke(obj, args);
            }
            catch (Exception)
            {
                error = "Overflow";
            }

            return 0;
        }
   
    }
}
