using System;

namespace Application
{
    public class Model<T>:IModel<T>
    {
        private IRepository<T> repository;
        private IRepository<T> db_repository;
        private ILowLevelFunc lowLevelFunc;
        private ICompiler compiler;
        private IRepository<T> currentRepository;
        private bool usedb = false;
        public Model(IRepository<T> repository, IRepository<T> db_repository, ILowLevelFunc lowLevelFunc, ICompiler compiler)
        {
            this.repository = repository;
            this.db_repository = db_repository;
            this.lowLevelFunc = lowLevelFunc;
            this.compiler = compiler;
            UseDB = false;
        }
        /// <summary>
        /// Использование базы в качестве репозитория
        /// </summary>
        public bool UseDB {
            get {
                return usedb;
            }
            set {
                usedb = value;
                currentRepository = usedb ? db_repository : repository;
            }
        }
        /// <summary>
        /// Инициализация репозитория модели
        /// </summary>
        /// <param name="config">Словарь-конфигуратор</param>
        public void init(RepositoryConfig config)
        {
            currentRepository.init(config);
        }
        /// <summary>
        /// Сохранение записей в реозитории
        /// </summary>
        /// <param name="config">Словарь-конфигуратор</param>
        public void save(RepositoryConfig config)
        {
            currentRepository.save(config);
        }
        /// <summary>
        /// Закрытиеь репозитория 
        /// </summary>
        public void close()
        {
            currentRepository.close();
        }
        /// <summary>
        /// Получить все записи из репозитория
        /// </summary>
        /// <returns></returns>
        public Record<T>[] getAllRecords()
        {
            return currentRepository.GetAllRecords();
        }
        /// <summary>
        /// Добавление записи
        /// </summary>
        /// <param name="rec">Добавляемая запись</param>
        public void addRecord(IStruct rec)
        {
            currentRepository.addRecord(new Record<T>((T)rec));
        }
        /// <summary>
        /// Редактирование записи
        /// </summary>
        /// <param name="rec">Запись</param>
        /// <param name="index"> Номер-id записи</param>
        public void editRecord(IStruct rec, int index)
        {
            currentRepository.editRecord(new Record<T>((T)rec), index);
        }
        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="index"></param>
        public void deleteRecord(int index)
        {
            currentRepository.deleteRecord(index);
        }
        /// <summary>
        /// Вызов метода интепретации
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string compile(string code)
        {
            return compiler.compile(code);
        }
        /// <summary>
        /// Вызов функции подсчета
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public ValueType calculate(ValueType a, ValueType b, out string error)
        {
            return lowLevelFunc.Function(a, b, out error);
        }
    }
}