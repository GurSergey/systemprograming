using System;
using System.Reflection;

namespace Application
{
    /// <summary>
    /// Класс предоставляющий возможность работать
    /// с библеотекой dll Варианта №7
    /// </summary>
    public class LowLevelFuncB : ILowLevelFunc
    {
        public static string dllName = "lowLevelFuncB";
        public static string className = "LowLevelFunc";
        public static string functionName = "Function";
        /// <summary>
        /// Подсчет значения
        /// </summary>
        /// <param name="a">Первый параметр</param>
        /// <param name="b">Второй параметр</param>
        /// <param name="error">Текст содержащий ошибку</param>
        /// <returns>Значение вычесленной функции</returns>
        public ValueType Function(ValueType a, ValueType b, out string error)
        {
            Assembly assembly = Assembly.Load(dllName);
            object obj = assembly.CreateInstance(className);
            Type type = assembly.GetType(className);
            MethodInfo method = type.GetMethod(functionName);

            object[] args = new object[2];
            args[0] = a;
            args[1] = b;
            error = "";
            //try
            //{
                return (int)method.Invoke(obj, args);
            //}
            //catch (Exception)
            //{
            //    error = "Overflow";
            //    Console.WriteLine("Overflow");
            //}

            return 0;
        }

    }
}
