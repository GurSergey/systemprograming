using System;
using System.Reflection;

namespace Application
{
    /// <summary>
    /// Класс обертка над структорой
    /// которая средствами рефлексии
    /// позволяет узнать информацию о полях структуры
    /// </summary>
    /// <typeparam name="T">тип структуры</typeparam>
    public class Record<T>
    {
        /// <summary>
        /// имя полей хранящихся в структуре
        /// </summary>
        public static string[] StructFieldNames { get; private set; }
        /// <summary>
        /// Типы полей в структуре
        /// </summary>
        public static Type[] StructFieldTypes { get; private set; }
        /// <summary>
        /// СТруктура хранящаяся в обертке
        /// </summary>
        public T RecordStruct { get; private set; }
        /// <summary>
        ///  статический Конструктор исследующий структуру
        /// </summary>
        static Record()
        {
            PropertyInfo[] fi = typeof(T).GetProperties();
            StructFieldNames = new string[fi.Length];
            StructFieldTypes = new Type[fi.Length];
            int i = 0;
            foreach (var field in fi)
            {
                Console.WriteLine((field.Name));
                Console.WriteLine(field.PropertyType.Name);
                StructFieldNames[i] = field.Name;
                StructFieldTypes[i] = field.PropertyType;
                i++;
            }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="record"></param>
        public Record(T record)
        {
            RecordStruct = record;
        }
    }
}