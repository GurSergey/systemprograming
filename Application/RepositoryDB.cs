﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Application
{
    /// <summary>
    /// Репозиторий для работы с базорй данных
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class RepositoryDB<T> : IRepository<T> where T : class
    {
        private List<Record<T>> list;
        RecordContext<T> db;
        /// <summary>
        /// Инициализация репозитория
        /// </summary>
        /// <param name="config"></param>
        public void init(RepositoryConfig config)
        {
            db = new RecordContext<T>();
            var records = db.Records;
            list = new List<Record<T>>();
            foreach (T rec in records)
            {
                list.Add(new Record<T>(rec));
            }
        }
        /// <summary>
        /// сохранение
        /// </summary>
        /// <param name="config"></param>
        public void save(RepositoryConfig config)
        {
            db.SaveChanges();
        }
        /// <summary>
        /// Закрытие
        /// </summary>
        public void close()
        {
            db.Dispose();
            if (list != null)
                list.Clear();
        }
        /// <summary>
        /// Добавление записи
        /// </summary>
        /// <param name="rec"></param>
        public void addRecord(Record<T> rec)
        {
            db.Records.Add(rec.RecordStruct);
        }
        /// <summary>
        /// Редактирование записи
        /// </summary>
        /// <param name="rec"></param>
        /// <param name="index"></param>
        public void editRecord(Record<T> rec, int index)
        {
            T listRecord = list[index].RecordStruct;
            Type recType = listRecord.GetType();
            int id = (int)recType.GetProperty("Id").GetValue(listRecord);
            T dbRecord = db.Records.Find(id);
            var properties = recType.GetProperties();
            foreach (var property in properties)
            {
                if (property.Name == "Id")
                    continue;
                object val = property.GetValue(rec.RecordStruct);
                property.SetValue(dbRecord, val);
            }
        }
        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="index">Номер запсии</param>
        public void deleteRecord(int index)
        {
            T record = list[index].RecordStruct;
            db.Records.Remove(record);
        }
        /// <summary>
        /// Получение всех записей
        /// </summary>
        /// <returns></returns>
        public Record<T>[] GetAllRecords()
        {
            return list.ToArray();
        }
    }
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class RecordContext<T> : DbContext where T : class
    {
        public RecordContext() : base("DbConnection") { }

        public DbSet<T> Records { get; set; }
    }
}
