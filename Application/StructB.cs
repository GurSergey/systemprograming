using System;
namespace Application
{
    /// <summary>
    /// Структура варианта №7
    /// </summary>
    public class StructB : IStruct
    {
        /// <summary>
        /// Конструктор без параметров
        /// </summary>
        public StructB() { }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="adress"></param>
        /// <param name="accessMode"></param>
        /// <param name="accessDate"></param>
        public StructB(string adress, string accessMode, DateTime accessDate)
        {
            Adress = adress;
            AccessMode = accessMode;
            AccessDate = accessDate;
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="adress"></param>
        /// <param name="accessMode"></param>
        /// <param name="accessDate"></param>
        public StructB(string adress, string accessMode, string accessDate)
        {
            Adress = adress;
            AccessMode = accessMode;
            AccessDate = DateTime.Parse(accessDate);
        }
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Адрес
        /// </summary>
        public string Adress { get; set; }
        /// <summary>
        /// Режим доступа
        /// </summary>
        public string AccessMode { get; set; }
        /// <summary>
        /// Дата доступа
        /// </summary>
        public DateTime AccessDate { get; set; }
    }
}