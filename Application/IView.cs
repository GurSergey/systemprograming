using System;
using System.Data;

namespace Application
{
    /// <summary>
    /// Интерфейс интерпретации
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Событие создания репозитория
        /// </summary>
        event EventHandler<FileEventArgs> reposCreated;
        /// <summary>
        /// Событие открытия репозитория
        /// </summary>
        event EventHandler<FileEventArgs> reposOpened;
        /// <summary>
        ///  Событие сохранения записей в репозиторий
        /// </summary>
        event EventHandler<FileEventArgs> reposSaved;
        /// <summary>
        /// Событие закрытия репозитория
        /// </summary>
        event Action reposClosed;
        /// <summary>
        /// Событие открытие базы данных
        /// </summary>
        event Action<int> dbOpened;
        /// <summary>
        /// Событие добавления записи
        /// </summary>
        event EventHandler<AddEventArgs> recordAdded;
        /// <summary>
        /// событие редактирования записи
        /// </summary>
        event EventHandler<EditEventArgs> recordEdited;
        /// <summary>
        /// событие удаления записи
        /// </summary>
        event EventHandler<DeleteEventArgs> recordDeleted;
        /// <summary>
        /// событие начала интерпретации
        /// </summary>
        event EventHandler<CompileEventArgs> compile;
        /// <summary>
        /// событие вызова функции из dll
        /// </summary>
        event EventHandler<CalculateEventArgs> calculate;
        /// <summary>
        /// метод обновление данных 
        /// </summary>
        /// <param name="result">строка с результатом</param>
        void updateCalculate(string result);
        /// <summary>
        /// Метод обновления лога 
        /// </summary>
        /// <param name="message"></param>
        void updateLog(string message);
        /// <summary>
        /// Метод обновления спсиска структур
        /// </summary>
        /// <param name="table"></param>
        void updateStructs(DataTable table);
        /// <summary>
        /// получение номера варианта
        /// </summary>
        int Variant { get; set; }
    }
}