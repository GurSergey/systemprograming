using System;
using System.Security.Cryptography;
using System.Text;

namespace Application
{
    [Serializable]
    public class StructG : IStruct
    {
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Хеш Код Пароля
        /// </summary>
        public string HashCode { get; set; }
        /// <summary>
        /// Электронная почта
        /// </summary>
        public string EMail { get; set; }

        public StructG() { }
        /// <summary>
        ///  Конструктор
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="e_mail"></param>
        public StructG(string login, string password, string e_mail)
        {
            Login = login;
            HashCode = GetHash(password);
            EMail = e_mail;
        }
        /// <summary>
        /// Метод получения хэша пароля по строке
        /// </summary>
        /// <param name="input">СТрока с паролем</param>
        /// <returns>Хеш код</returns>
        private string GetHash(string input)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
 
            return Convert.ToBase64String(hash);
        }
    }
}