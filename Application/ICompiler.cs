namespace Application
{
    /// <summary>
    /// Интерфейс имнтерпретатора
    /// </summary>
    public interface ICompiler
    {
        /// <summary>
        /// метод интерпретации
        /// </summary>
        /// <param name="code">код для интепретации</param>
        /// <returns></returns>
        string compile(string code);
    }
}