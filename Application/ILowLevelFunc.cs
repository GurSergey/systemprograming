using System;

namespace Application
{
    /// <summary>
    /// Класс предоставляющий возможность работать
    /// с библеотекой dll Для подсчета значений
    /// </summary>
    public interface ILowLevelFunc
    {
        /// <summary>
        /// Подсчет значения
        /// </summary>
        /// <param name="a">Первый параметр</param>
        /// <param name="b">Второй параметр</param>
        /// <param name="error">Текст содержащий ошибку</param>
        /// <returns>Значение вычесленной функции</returns>
        ValueType Function(ValueType a, ValueType b, out string error);
    }

}