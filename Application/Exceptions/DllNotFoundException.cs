using System;

namespace Application.Exceptions
{
    public class DllNotFoundException:ApplicationException
    {
        DllNotFoundException(String name)
        {
            this.message = "Модуль dll " + name + " не найден";
        }
    }
}