using System;

namespace Application.Exceptions
{
    /// <summary>
    /// Класс предок всех исключений логики приложения
    /// </summary>
    public class ApplicationException: Exception
    {
        protected string message;

        public string GetMessage()
        {
            return message;
        }
    }
}