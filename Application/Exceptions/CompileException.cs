using System;

namespace Application.Exceptions
{
    public class CompileException:ApplicationException
    {
        CompileException(String text)
        {
            this.message = text;
        }
    }
}