using System;

namespace Application.Exceptions
{
    public class NotFoundInConfig:ApplicationException
    {
        NotFoundInConfig(String key)
        {
            this.message = "Ключ" + key + " не найден в конфигурационном словаре";
        }
    }
}