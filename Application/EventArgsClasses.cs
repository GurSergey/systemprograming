using System;

namespace Application
{
    /// <summary>
    /// Класс событие открытия репозитория
    /// </summary>
    public class FileEventArgs : EventArgs
    {
        public FileEventArgs(RepositoryConfig conf)
        {
            Config = conf;
        }

        public RepositoryConfig Config { get; private set; }
    }
    /// <summary>
    /// Добавить запись
    /// </summary>
    public class AddEventArgs : EventArgs
    {
        public AddEventArgs(IStruct struc)
        {
            Structure = struc;
        }

        public IStruct Structure { get; private set; }
        public int Pos { get; private set; }
    }
    /// <summary>
    /// Класс событие редактирование записи
    /// </summary>
    public class EditEventArgs : EventArgs
    {
        public EditEventArgs(IStruct struc, int pos)
        {
            Structure = struc;
            Pos = pos;
        }

        public IStruct Structure { get; private set; }
        public int Pos { get; private set; }
    }
    /// <summary>
    /// Класс событие удаление записи
    /// </summary>
    public class DeleteEventArgs : EventArgs
    {
        public DeleteEventArgs(int pos)
        {
            Pos = pos;
        }

        public int Pos { get; private set; }
    }

    /// <summary>
    /// Класс событие интепретации
    /// </summary>
    public class CompileEventArgs : EventArgs
    {
        public CompileEventArgs(string code)
        {
            Code = code;
        }

        public string Code { get; private set; }
    }
    /// <summary>
    /// Класс событие вычисление низкоуровневой функции
    /// </summary>
    public class CalculateEventArgs : EventArgs
    {
        public CalculateEventArgs(ValueType a, ValueType b)
        {
            A = a;
            B = b;
        }

        public ValueType A { get; private set; }
        public ValueType B { get; private set; }
    }
}