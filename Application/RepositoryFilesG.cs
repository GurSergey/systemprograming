using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Runtime.Serialization.Formatters.Binary;

namespace Application
{
    /// <summary>
    /// Репозиторий для работы с файлами вариант №9
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RepositoryFilesG<T>: IRepository<T>
    {
        private const string paramFilePath = "FileName";
        private string filePath;
        private List<Record<T>> list;

        public RepositoryFilesG()
        {
              
        }

        public void init(RepositoryConfig config)
        {
            list = new List<Record<T>>();
            filePath = config.getParameter(paramFilePath);
            var formatter = new BinaryFormatter();

            if (!File.Exists(filePath))
            {
                //BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                {
                    formatter.Serialize(fs, new T[0]);
                }
            }

            using (FileStream fs = new FileStream(filePath, FileMode.Open))
            {
                var arr = (T[]) formatter.Deserialize(fs);
                foreach (var elem in arr)
                {
                    list.Add(new Record<T>(elem));
                }
            }
        }

        public void save(RepositoryConfig config)
        {
            filePath = config.getParameter(paramFilePath);
            T[] arr = new T[list.Count];
            int i = 0;
            foreach (var elem in list)
            {
                arr[i] = elem.RecordStruct;
                i++;
            }
            
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                formatter.Serialize(fs, arr);
            }
        }

        public void close()
        {
            filePath = "";
            if (list != null)
                list.Clear();
        }

        public Record<T>[] GetAllRecords()
        {
            return list.ToArray();
        }

        public void addRecord(Record<T> rec)
        {
            list.Add(rec);
        }

        public void editRecord(Record<T> rec, int index)
        {
            list.RemoveAt(index);
            list.Insert(index, rec);
        }

        public void deleteRecord(int index)
        {
            list.RemoveAt(index);
        }
    }
}