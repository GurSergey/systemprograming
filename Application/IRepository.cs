namespace Application
{
    /// <summary>
    /// Интерфейс Репозиторий
    /// Позволяет единообразно работать с
    /// хранилищами
    /// </summary>
    /// <typeparam name="T">Тип сохраняемой структруры</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Инциализация репозитория
        /// </summary>
        /// <param name="config"> Словарь-конфигурация</param>
        void init(RepositoryConfig config);
        /// <summary>
        /// Сохранение данных
        /// Хранящихся промежуточно
        /// в оперативной памяти в репозиторий
        /// </summary>
        /// <param name="config"></param>
        void save(RepositoryConfig config);
        /// <summary>
        /// Закрытие репозитория
        /// </summary>
        void close();
        /// <summary>
        /// Добавление записи в  репозитоий
        /// </summary>
        /// <param name="rec">запись</param>
        void addRecord(Record<T> rec);
        /// <summary>
        /// Редактирование записи в репозитории
        /// </summary>
        /// <param name="rec">запись</param>
        /// <param name="pos">id записи</param>
        void editRecord(Record<T> rec, int pos);
        /// <summary>
        /// Удаление записи из репозитоиия
        /// </summary>
        /// <param name="id">id записи</param>
        void deleteRecord(int id);
        /// <summary>
        /// Получить список всех записей
        /// </summary>
        /// <returns></returns>
        Record<T>[] GetAllRecords();
    }
}